$("#btn-questao").click(function (event) {
    var id = user.id;
    var tip = $("#tipo_pergunta-questao").val();
    var num = $("#num_alternativas-questao").val();
    var enu = $("#enunciado-questao").val();
    var res = $("#resposta-questao").val();
    var alt = getAlternativas(num);
    var dis = $("#disciplina-questao").val();
    var top = $("#topico-questao").val();

    console.log(tip, num, enu, res, alt, dis, top);

    if (!dis || !top) {
        console.log('dis ou top invalidos');
        M.toast({html: 'Preencha todos os campos', displayLength: 1000});
        return;
    }

    if (!validate()) {
        console.log('erro validacao 1');
        M.toast({html: 'Preencha todos os campos', displayLength: 1000});
        return;
    }

    if (tip == '1') {
        if (!validateAlternativas(num, alt)) {
            console.log('alt invalidas');
            M.toast({html: 'Preencha todos os campos', displayLength: 1000});
            return;
        }
    }

    //id_usuario: id, tipo_pergunta: tip, enunciado: enu, resposta: res, disciplina: dis, topico: top, num_alternativas: num, alternativas: alt
    var dados = {
        id_usuario: id,
        tipo_pergunta: tip,
        enunciado: enu,
        resposta: res,
        disciplina: dis,
        topico: top,
        num_alternativas: num,
        alternativas: alt
    };

    console.log(dados);
    // return;

    $.ajax({
        url: host + 'pergunta/criarPergunta',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(dados),
        headers: {
            'Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjcyNjc2NDcsImRhdGEiOnsibnVtZXJvIjoiOTk5OTk5OTk5OTkiLCJjb2RpZ28iOiIwMDAwMDEifX0.xZ-CE0pwLa3e313RqhWiumbhEjcRRIMEY6sntKoXN8M'
        },
    })
        .done(function (d) {
            console.log("success");
            console.log(d);
            M.toast({html: 'Pergunta criada com sucesso', displayLength: 1000});
            setTimeout(function (argument) {
                $(document).trigger('hashchange');
            }, 1000);
        })
        .fail(function (d) {
            console.log("error");
            M.toast({html: 'Erro ao criar pergunta', displayLength: 1000});
            console.log(d);
        })
        .always(function () {
            // console.log("complete");
        });


});

function getAlternativas(num) {
    var alt = {};
    for (var i = 1; i <= num; i++) {
        alt['alternativa_' + i] = $('#alternativa_' + i).val();
    }
    return alt;
}

function validateAlternativas(num, alt) {
    t = true;
    $.each(alt, function (index, el) {
        console.log();
        if (el == "") {
            t = false;
            return;
        }
    });
    return t;
}

function criarAlternativas(numero) {
    if (numero < 3 || numero > 5)
        return;
    $("#alternativas-questao").html('');
    for (var i = 1; i <= numero; i++) {
        // var alt = '<div><label>Alternativa '+i+'</label><input id="alternativa_'+i+'" type="text" required></div>';
        var alt = '<div class="input-field"><textarea id="alternativa_' + i + '" class="materialize-textarea" required></textarea><label for="alternativa_' + i + '">Alternativa ' + i + '</label></div>';
        $("#alternativas-questao").append(alt);
    }
    $("#alternativas-questao").removeClass('hide');
}

function criarRespostas() {
    var selectResposta = $("#resposta-questao");
    selectResposta.find('option').remove().end().append('<option selected disabled>Resposta</option>');
    var tipo = $("#tipo_pergunta-questao").val();
    if (tipo == '1') {
        var qtd = $("#num_alternativas-questao").val();
        if (!qtd)
            return;
        for (var i = 1; i <= qtd; i++) {
            selectResposta.append(new Option(i, i));
        }
    } else if (tipo == '2') {
        selectResposta.append(new Option('Verdadeiro', 1));
        selectResposta.append(new Option('Falso', 2));
    }
    var selects = document.querySelectorAll('select');
    var instances = M.Select.init(selects);
}


$("#tipo_pergunta-questao").change(function (event) {
    var tipo = $(this).val();
    if (tipo == '1') {
        $("#container_alternativas-questao").removeClass('hide');
    } else if (tipo == '2') {
        $("#container_alternativas-questao").addClass('hide');
        $("#alternativas-questao").addClass('hide');
    }
    criarRespostas();
});

$("#num_alternativas-questao").change(function (event) {
    var num = $(this).val();
    criarAlternativas(num);
    criarRespostas();
});

function in_array(val, arr) {
    var t = false;
    $.each(arr, function (index, el) {
        if (val === el) {
            t = true;
            return;
        }
    });
    return t;
}

// var g = ['1', 'yt', true];
// console.log(in_array('1', g));

function validate() {
    var enun = $("#enunciado-questao").val();
    if (!enun)
        return false;
    var res = $("#resposta-questao").val();
    var tip = $("#tipo_pergunta-questao").val();
    if (!tip)
        return false;
    console.log('tipo valido');

    var tipo = $("#tipo_pergunta-questao").val();
    if (tipo == '1') {
        console.log('tipo 1');
        var num_alternativas = $("#num_alternativas-questao").val();
        if (num_alternativas != '3' && num_alternativas != '4' && num_alternativas != '5') {
            console.log('erro num');
            return false;
        }
        if (res < 0 || res > num_alternativas || !res)
            return false;

        return true;
    } else if (tipo == 2) {
        console.log('tipo 2');
        if (res != 1 && res != 2)
            return false;

        return true;
    } else {
        return false;
    }
}


$.get(host + 'escola/getDisciplinas', function (data) {
    data = convertToJSON(data);
    $.each(data, function (index, el) {
        $("#disciplina-questao").append(new Option(el.nome, el.id));
    });
    var selects = document.querySelectorAll('select');
    var instances = M.Select.init(selects);
});


$("#disciplina-questao").change(function (argument) {
    var dis = $(this).val();
    $('#topico-questao').find('option').remove().end().append('<option selected disabled>Tópico</option>');
    $.get(host + 'escola/getTopicosByDisciplina?id_disciplina=' + dis, function (data) {
        data = JSON.parse(data);
        console.log(data);
        $.each(data, function (index, el) {
            $("#topico-questao").append(new Option(el.nome, el.id));
        });
        var selects = document.querySelectorAll('select');
        var instances = M.Select.init(selects);
    });
});

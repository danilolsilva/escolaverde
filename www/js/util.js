function adicionarHistorico(url) {
    if (myHistory[myHistory.length - 1] !== url && $.inArray(url.split('?')[0], paginas_nao_entrar_historico) !== -1)
        myHistory.push(url);
    console.log('Histórico atual:', myHistory);
}

function validateUser() {
    var user = localStorage.getItem('usuario');
    var token = localStorage.getItem('token');
    return user !== null && user !== undefined && token !== null && token !== undefined;
}

function hideMenu() {
    $("#nav").addClass('hide');
}

function showMenu() {
    $("#nav").removeClass('hide');
}

function newTemplate(caminhoTemplateFile, dadosParaPreencher, elementoHtmlPreencher) {
    $.ajax({
        url: caminhoTemplateFile,
        success: function (d) {
            var tmplt = $.templates(d);
            var html;
            if (dadosParaPreencher != null)
                html = tmplt.render(dadosParaPreencher);
            else
                html = tmplt.render();
            elementoHtmlPreencher.html(html);
        }
    });
}

function toDataURL(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        var reader = new FileReader();
        reader.onloadend = function () {
            callback(reader.result);
        };
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
}

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function logout() {
    //deletePushIdOnServer(localStorage.getItem('registrationId'));
    localStorage.removeItem('token');
    localStorage.removeItem('usuario');
    localStorage.removeItem('user');
    myHistory = [];
    $(window).trigger('hashchange');
}

function convertToJSON(d) {
    if (d === null)
        return {};
    try {
        if (typeof d !== 'object')
            return JSON.parse(d);
        return d;
    } catch (e) {
        return {};
    }
}

function setTempItem(chave, valor) {
    var tmp = convertToJSON(localStorage.getItem('tmp_data'));
    tmp[chave] = valor;
    localStorage.setItem('tmp_data', JSON.stringify(tmp));
}

function getTempData(chave) {
    var tmp = convertToJSON(localStorage.getItem('tmp_data'));
    return tmp[chave];
}

// function setFotoPerfil() {
//     var user = convertToJSON(localStorage.getItem('usuario'));
//     var d = new Date();
//     if (user.foto_perfil !== null && user.foto_perfil !== 'null') {
//         var foto = host + 'app/showImage?nome=' + user.foto_perfil + '&t=' + d.getTime();
//         console.log('foto');
//         console.log(foto);
//         $('#div-foto-inicio').css("background-image", "url('" + host + 'app/showImage?nome=' + user.foto_perfil + '&t=' + d.getTime() + "')");
//         $('#foto-perfil-menu').css("background-image", "url('" + host + 'app/showImage?nome=' + user.foto_perfil + '&t=' + d.getTime() + "')");
//     } else {
//         $('#div-foto-inicio').css("background-image", "url('/img/default-user-image.png')");
//         $('#foto-perfil-menu').css("background-image", "url('/img/default-user-image.png')");
//     }
// }

function getUserData() {
    return convertToJSON(localStorage.getItem('usuario'));
}

function getUser() {
    return convertToJSON(localStorage.getItem('usuario'));
}

function playVideo(videoUrl) {
    var options = {
        mustWatch: true,
        successCallback: function () {
            console.log("Video was closed without error.");
        },
        errorCallback: function (errMsg) {
            console.log("Error! " + errMsg);
        }
    };
    window.plugins.streamingMedia.playVideo(videoUrl, options);
}

function showleitorQr() {
    $("#nav").addClass('hide');
    $("#container").addClass('hide');
    $("#container-qr-code").removeClass('hide');
}

function hideleitorQr() {
    $("#nav").removeClass('hide');
    $("#container").removeClass('hide');
    $("#container-qr-code").addClass('hide');
}

function getFotoPerfilFromDB(nome_foto) {
    var d = new Date();
}

function setarUsuario(user) {
    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('usuario', JSON.stringify(user));
}

function convertFotoPerfilBackgroundImg(val) {
    if (val.fotoPerfil)
        return "style=\"background-image: url('" + getUrlFotoPerfil(val) + "')\"";
    else
        return "";
}
function convertFotoPerfilBackgroundImgFT(val) {
    if (val)
        return "style=\"background-image: url('" + getUrlFotoPerfilByFotoPerfil(val) + "')\"";
    else
        return "";
}

var user = getUser();

function showToast(msg) {
    if (msg === undefined)
        msg = "";
    M.toast({html: msg, displayLength: 1500});
}

function tratarRequestErro(response) {
    console.log("Erro: ", response);
    //showToast(response.responseText);
    try {
        window.plugins.toast.showWithOptions(
            {
                message: response.responseText,
                duration: "short",
                position: "bottom",
                addPixelsY: -40
            }
        );
    } catch (e) {
        console.log('toast não deu ', e);
    }
    var code = response.status;
    if (code === 401)
        logout();
    renderizarPagina(window.location.hash.substr(1));
}

function autenticar() {
    $.ajax({
        url: host + 'app/autenticar-usuario',
        type: 'GET',
        dataType: 'json'
    })
        .done(function (d) {
            var dataJSON = convertToJSON(d);
            if (dataJSON.user !== null) {
                setarUsuario(dataJSON.user);
                renderMenuLateral(getUser());
                $("#nome").html(getUser().nome);
                //$("#nome-menu").html(user.nome);
            }
            renderizarPagina(window.location.hash.substr(1));
        })
        .fail(function (d) {
            tratarRequestErro(d);
        })
        .always(function (d) {
            console.log("complete", d);
        });
}

function renderMenuLateral(user) {
    var tmpl = $.templates("#template-menu-lateral");
    var html = tmpl.render(user);
    $("#container-menu-lateral").append(html);
    $('.sidenav').sidenav();
}

function removeMenuLateral() {
    $("#container-menu-lateral").html("");
}

function getUrlFotoPerfil(user) {
    return host + 'file/profile-picture?id=' + user.fotoPerfil.id + '&nome=' + user.fotoPerfil.nome;
}


function getUrlFotoPerfilByFotoPerfil(fotoPerfil) {
    if (fotoPerfil !== null && fotoPerfil != undefined)
        return host + 'file/profile-picture?id=' + fotoPerfil.id + '&nome=' + fotoPerfil.nome;
    return "";
}


$.views.helpers("nenhumaBatalha", function(size_aguardando, size_suavez, size_finalizadas) {
	return (size_aguardando == 0 && size_suavez == 0 && size_finalizadas == 0);
});

$.views.helpers("isUser", function(id) {
	return parseInt(id) === parseInt(getUserData().id);
});

$.views.helpers('testMidiaIsImage', function (val) {
	return /^image\/[a-z0-9]{3,4}$/.test(val);
});

$.views.helpers('isUser', function (val) {
	return val == user.id;
});

$.views.helpers('getUser', function () {
	return getUserData();
});


$.views.helpers('getUrlFotoPerfil', function (fotoPerfil) {
	return getUrlFotoPerfilByFotoPerfil(fotoPerfil);
});

var app = {
    initialize: function () {
        this.bindEvents();
    },
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    onDeviceReady: function () {
        console.log('deviceready event');
        $('.sidenav').sidenav();
        $('.collapsible').collapsible();
    },
    registerPush: function (endRegister) {
        // console.log('Chamou a função register push');
        // var notificationOpenedCallback = function (jsonData) {
        //     console.log('Clica na not: ' + (jsonData));
        // };
        //
        // var notificationJaAberto = function (jsonData) {
        //     console.log('App aberto: ' + (jsonData));
        // };
        //
        // function registerPushIdOnServer(push_id) {
        //     $.ajax({
        //         url: host + 'push/addPushId',
        //         type: 'POST',
        //         dataType: 'json',
        //         data: JSON.stringify({
        //             id_usuario: getUserData().id, id_push: push_id
        //         }),
        //     })
        //         .done(function (d) {
        //             endRegister(d);
        //             // var datajson = convertToJSON(d);
        //             // console.log("push id registrado no server backend:", datajson);
        //             // myHistory = [];
        //             // location.hash = 'inicio';
        //             // window["plugins"].OneSignal.setSubscription(true);
        //         })
        //         .fail(function (d) {
        //             console.log("register push id backend error:", d);
        //             myHistory = [];
        //             location.hash = 'inicio';
        //         });
        //
        // }
        //
        // window.plugins.OneSignal
        //     .startInit("2ad4c3d0-aec9-46db-9ad8-490d5afd6771")
        //     .handleNotificationReceived(notificationJaAberto)
        //     .handleNotificationOpened(notificationOpenedCallback)
        //     .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
        //     .endInit();
        //
        // window.plugins.OneSignal.getIds(function (ids) {
        //     window["plugins"].OneSignal.setSubscription(true);
        //     var id = ids.userId;
        //     localStorage.setItem('push_id', id);
        //     console.log('ID do onesignal:', id);
        //     registerPushIdOnServer(id);
        // });
    },
    deletePush: function (argument) {
        // console.log('Chamou a função delete push');
        // window.plugins.OneSignal.getIds(function (ids) {
        //     window["plugins"].OneSignal.setSubscription(false);
        //     var id = ids.userId;
        //     localStorage.setItem('push_id', id);
        //     $.ajax({
        //         url: host + 'push/deletePushId',
        //         data: {push_id: id},
        //     })
        //         .done(function (d) {
        //             var datajson = convertToJSON(d);
        //             console.log("deletado push id:", datajson);
        //         })
        //         .fail(function (d) {
        //             console.log("delete push id error:", d);
        //         })
        //         .always(function () {
        //             console.log("complete delete push id backend");
        //
        //         });
        // });
    }
};

app.initialize();

var container = $('#container');

var myHistory = [];

$(window).on('hashchange', function (e) {
    console.log(decodeURI(window.location.hash));
});

$.ajaxSetup({
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Token", localStorage.getItem('token'));
        //xhr.setRequestHeader("TokenUserId", localStorage.getItem('TokenUserId'));
    }
});

var xhrQueue = [];

$(document).ajaxSend(function (event, jqxhr, settings) {
    if (settings.url.split('?')[0] !== (host + 'push/deletePushId'))
        xhrQueue.push(jqxhr);
});

$(document).ajaxComplete(function (event, jqxhr, settings) {
    var i;
    if ((i = $.inArray(jqxhr, xhrQueue)) > -1) {
        xhrQueue.splice(i, 1);
    }
});

ajaxAbort = function () {
    console.log('abortando ', xhrQueue.length, 'ajax requests...');
    try {
        var i = 0;
        while (xhrQueue.length) {
            xhrQueue[i++].abort();
        }
        console.log('ajax requests abortadas');
    } catch (e) {
        console.log('erro ao abortar ajax');
    }
};

var lastTimeBackPress = 0;
var timePeriodToExit = 2000;
var botaoVoltarAtivado = true;

function onBackKeyDown(e) {
    e.preventDefault();
    e.stopPropagation();
    console.log('tamanho do histórico: ', myHistory.length);
    if (!botaoVoltarAtivado)
        return;
    if (myHistory[myHistory.length - 2] !== undefined) {
        window.location.hash = myHistory[myHistory.length - 2];
        myHistory.splice(-1, 2);
        console.log(myHistory);
    } else {
        if (new Date().getTime() - lastTimeBackPress < timePeriodToExit) {
            navigator.app.exitApp();
        } else {
            window.plugins.toast.showWithOptions(
                {
                    message: "Pressione novamente para sair",
                    duration: "short",
                    position: "bottom",
                    addPixelsY: -40
                }
            );
            lastTimeBackPress = new Date().getTime();
        }
    }
}

document.addEventListener("backbutton", onBackKeyDown, false);




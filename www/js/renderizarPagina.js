function renderBatalhas_1() {
    $.views.helpers('isSuaVez', function (status_batalha, vez) {
        if (status_batalha == 1 && vez == user.id)
            return true;
        else
            return false;
    });
    $.views.helpers('isAguardando', function (status_batalha, vez) {
        if (status_batalha == 1 && vez != user.id)
            return true;
        else
            return false;
    });
    $.views.helpers('isFinalizada', function (status_batalha) {
        return status_batalha == 2;
    });
    $.views.converters("formataData", function (val) {
        moment.locale('pt-br');
        // console.log('como ele retorna', moment().format('MMMM Do YYYY, h:mm:ss a'));
        return moment(val).format('DD/MM/YYYY HH:mm');
    });
    $.ajax({
        url: host + 'batalha/getBatalhasByUser',
        type: 'GET',
        data: {id: getUserData().id},
    })
        .done(function (d) {
            var data = convertToJSON(d);
            console.log(data);
            newTemplate('templates/batalhas_1.html', {'data': data}, container);
            console.log("success");
        })
        .fail(function (d) {
            console.log(d);
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });

    // newTemplate('templates/batalhas_1.html', null, container);
}


function renderRevisarBatalha(dados) {
    $.views.helpers("isOponente1", function (val) {
        return user.id == val;
    });
    $.ajax({
        url: host + 'batalha/getBatalhaById',
        data: {id: dados},
    })
        .done(function (d) {
            console.log("success");
            var data = convertToJSON(d);
            console.log('dados batalha:', data);
            newTemplate('templates/revisar-batalha.html', data, container);
        })
        .fail(function (d) {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
}

function renderPerfil_1(dados) {

    $.views.helpers('testMidiaIsImage', function (val) {
        return /^image\/[a-z0-9]{3,4}$/.test(val);
    });

    $.ajax({
        url: host + 'app/getTimelineDataPerfil',
        type: 'GET',
        data: {id_usuario: getUserData().id, id_perfil: dados},
    })
        .done(function (d) {
            var data = convertToJSON(d);
            console.log(data);
            newTemplate('templates/perfil_1.html', data, container);
            console.log("success");
        })
        .fail(function (d) {
            console.log(d);
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });


    // newTemplate('templates/perfil.html', null, container);
}

function renderEditarNomeFoto_1() {
    newTemplate('templates/confirmar-nome-foto.html', null, container);
}

function renderControleDosPais_1() {
    $.ajax({
        url: host + 'app/getControleDosPaisData',
        data: {id: user.id},
    })
        .done(function (d) {
            console.log("success");
            var data = convertToJSON(d);
            console.log('dados retornados:', data);
            newTemplate('templates/controle-dos-pais_1.html', data, container);
        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });


}

function renderDesafiarQuestoes_1() {
    newTemplate('templates/desafiar-questoes.html', null, container);
}

function renderFimBatalha_1(dados) {
    console.log('dados:', dados);
    var id_b = dados.split('&')[0];
    var certas = dados.split('&')[1];
    console.log('split', id_b, certas);
    $.views.helpers('isOpo1', function (val) {
        return user.id == val;
    });
    $.views.helpers('isEmAndamento', function (val) {
        return val == 1;
    });
    $.ajax({
        url: host + 'batalha/getBatalhaById',
        data: {id: dados},
    })
        .done(function (d) {
            // console.log("success:", d);
            var data = convertToJSON(d);
            console.log('Batalha by id:', data);
            data.certas = certas;
            newTemplate('templates/fim-batalha_1.html', data, container);
        })
        .fail(function (d) {
            console.log("error:", d);
            alert('error');
        })
        .always(function () {
            console.log("complete");
        });


}

function renderMaterialDeEstudo_1() {
    $.ajax({
        url: host + 'app/getMaterialDeEstudoData',
        data: {id: user.id},
    })
        .done(function (d) {
            var data = convertToJSON(d);
            console.log('dados recebidos:', data);
            newTemplate('templates/material-de-estudo_1.html', data, container);
        })
        .fail(function (d) {
            console.log("error:", d);
            alert('error');
        })
        .always(function () {
            console.log("complete");
        });


}

function renderMinhaEscola_1() {
    $.ajax({
        url: host + 'app/getMinhaEscolaData',
        data: {id: user.id},
    })
        .done(function (d) {
            console.log("success");
            var data = convertToJSON(d);
            console.log('dados retornados:', data);
            newTemplate('templates/minha-escola_1.html', data, container);
        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });


}

function renderPergunta_1(dados) {
    $.views.converters("tostr", function (val) {
        return JSON.stringify(val);
    });

    $.ajax({
        url: host + 'batalha/iniciarBatalha',
        type: 'GET',
        data: {id_desafiante: getUserData().id, id_desafiado: dados},
    })
        .done(function (d) {
            var data = convertToJSON(d);
            // console.log(d);
            console.log(data);
            botaoVoltarAtivado = false;
            newTemplate('templates/pergunta_1.html', data.data, container);
            console.log("success");
        })
        .fail(function (d) {
            console.log(d);
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });


}

function renderContinuarBatalha(dados) {
    $.views.converters("tostr", function (val) {
        return JSON.stringify(val);
    });
    $.ajax({
        url: host + 'batalha/continuarBatalha',
        type: 'GET',
        data: {id_batalha: dados, id_usuario: user.id},
    })
        .done(function (d) {
            var data = convertToJSON(d);
            console.log(d);
            console.log('continuar batalha:', data);
            botaoVoltarAtivado = false;
            newTemplate('templates/continuar-batalha.html', data, container);
            console.log("success");
        })
        .fail(function (d) {
            console.log(d);
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });

    // newTemplate('templates/continuar-batalha.html', null, container);
}

function renderProcurandoOponente_1() {
    newTemplate('templates/procurando-oponente.html', null, container);
}

function renderResultadoDesafioQuestoes_1() {
    newTemplate('templates/resultado-desafio-questoes.html', null, container);
}

function renderListaConteudos(dados) {

    $.ajax({
        url: host + 'conteudo/getConteudosByTopicoNivelSerieDisc',
        data: {
            id_topico: dados,
            id_nivel: nivel_conteudo,
            id_serie: serie_conteudo,
            id_disciplina: disciplina_conteudo
        },
    })
        .done(function (d) {
            console.log("success");
            var data = convertToJSON(d);
            console.log('resposta json:', data);
            newTemplate('templates/lista-conteudos.html', {'id': dados, 'data': data}, container);
        })
        .fail(function (d) {
            console.log("error:", d);
        })
        .always(function () {
            console.log("complete");
        });


}


function renderInicializando() {
    newTemplate('templates/inicializando.html', null, container);
}

function renderCriarPerfil() {
    newTemplate('templates/criar-perfil.html', null, container);
}

function renderBemVindo() {
    newTemplate('templates/bem-vindo.html', null, container);
}

function renderBatalhas() {
    newTemplate('templates/batalhas.html', null, container);
}

function renderPerfil() {
    newTemplate('templates/perfil.html', null, container);
}


function renderControleDosPais() {
    newTemplate('templates/controle-dos-pais.html', null, container);
}

function renderDesafiarQuestoes(dados) {
    $.views.converters("tostr", function (val) {
        return JSON.stringify(val);
    });

    $.ajax({
        url: host + 'pergunta/desafiarPerguntasByDisciplina',
        type: 'GET',
        data: {id_disciplina: dados},
    })
        .done(function (d) {
            var data = convertToJSON(d);
            console.log(data);
            botaoVoltarAtivado = true;
            newTemplate('templates/desafiar-questoes.html', data.data, container);
            console.log("success");
        })
        .fail(function (d) {
            console.log(d);
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });


    // newTemplate('templates/desafiar-questoes.html', null, container);
}

function renderFimBatalha() {
    newTemplate('templates/fim-batalha.html', null, container);
}

function renderFimBatalhaParcial() {
    newTemplate('templates/fim-batalha-parcial.html', null, container);
}

function renderMaterialDeEstudo() {
    newTemplate('templates/material-de-estudo.html', null, container);
}

function renderMinhaEscola() {
    newTemplate('templates/minha-escola.html', null, container);
}

function renderPergunta() {
    newTemplate('templates/pergunta.html', null, container);
}

function renderProcurandoOponente() {
    newTemplate('templates/procurando-oponente.html', null, container);
}

function renderResultadoDesafioQuestoes() {
    newTemplate('templates/resultado-desafio-questoes.html', null, container);
}


/*********************************************************************************
 ##################################################################################
 **********************************************************************************/

function renderizarPagina(url) {
    console.log('\n################# Hashchange:', url, '##################\n\n');
    adicionarHistorico(url);
    botaoVoltarAtivado = true;
    var urlArr = url.split("?");
    url = urlArr[0];
    var dados = urlArr[1] !== 'undefined' ? urlArr[1] : null;
    console.log('Dados para a página renderizada:', dados);
    container.html(loadingTemplate.render());



    var renderPaginasMap = {
        '': function () {
            if (!validateUser())
                renderApresentacao();
            else
                renderInicio();
        },
        'apresentacao': function () {
            renderApresentacao();
        },
        'inserir-numero': function () {
            renderInserirNumero();
        },
        'confirmar-codigo': function () {
            renderConfirmarCodigo({numero: dados.replace('%20', ' ')});
        },
        'selecionar-usuario-ieducar': function () {
            renderSelecionarUsuarioIeducar();
        },
        'inicializando': function () {
            renderInicializando();
        },
        'selecionar-escola': function () {
            renderSelecionarEscola();
        },
        'criar-perfil': function () {
            renderCriarPerfil();
        },
        'editar-nome-foto': function () {
            renderEditarNomeFoto();
        },
        'bem-vindo': function () {
            renderBemVindo();
        },
        'inicio': function () {
            renderInicio();
        },
        'batalhas': function () {
            renderBatalhas();
        },
        'perfil': function () {
            renderPerfil();
        },
        'controle-dos-pais': function () {
            renderControleDosPais();
        },
        'desafiar-questoes': function () {
            renderDesafiarQuestoes(dados);
        },
        'fim-batalha': function () {
            renderFimBatalha();
        },
        'fim-batalha-parcial': function () {
            renderFimBatalhaParcial();
        },
        'material-de-estudo': function () {
            renderMaterialDeEstudo();
        },
        'minha-escola': function () {
            renderMinhaEscola();
        },
        'pergunta': function () {
            renderPergunta();
        },
        'procurando-oponente': function () {
            renderProcurandoOponente();
        },
        'resultado-desafio-questoes': function () {
            renderResultadoDesafioQuestoes();
        },
        'inicio_1': function () {
            renderInicio();
        },
        'batalhas_1': function () {
            renderBatalhas_1(dados);
        },
        'perfil_1': function () {
            renderPerfil_1(dados);
        },
        'controle-dos-pais_1': function () {
            renderControleDosPais_1();
        },
        'desafiar-questoes_1': function () {
            renderDesafiarQuestoes_1();
        },
        'fim-batalha_1': function () {
            renderFimBatalha_1(dados);
        },
        'material-de-estudo_1': function () {
            renderMaterialDeEstudo_1();
        },
        'minha-escola_1': function () {
            renderMinhaEscola_1();
        },
        'pergunta_1': function () {
            renderPergunta_1(dados);
        },
        'procurando-oponente_1': function () {
            renderProcurandoOponente_1();
        },
        'continuar-batalha': function () {
            renderContinuarBatalha(dados);
        },
        'revisar-batalha': function () {
            renderRevisarBatalha(dados);
        },
        'lista-conteudos': function () {
            renderListaConteudos(dados);
        }
    };

    // Execute the needed function depending on the url keyword (stored in temp).
    if (renderPaginasMap[url]) {
        renderPaginasMap[url]();
        $('html,body').scrollTop(0);
    } else {
        alert('página não existe.');
        location.hash = '';
    }
}

// #######################################################################################################################


function renderApresentacao() {
    removeMenuLateral();
    newTemplate('templates/apresentacao-logo.html', null, container);
}

function renderInserirNumero() {
    removeMenuLateral();
    newTemplate('templates/inserir-numero.html', null, container);
}

function renderConfirmarCodigo(data) {
    newTemplate('templates/confirmar-codigo.html', data, container);
}

function renderSelecionarUsuarioIeducar() {
    newTemplate('templates/selecionar-usuario-ieducar.html', null, container);
}

function renderSelecionarEscola() {
    $.ajax({
        url: host + 'estado/getAllEstados',
        type: 'GET',
        success: function (d) {
            console.log(d);
            var res = d;
            if (typeof d === 'string' || d instanceof String)
                res = JSON.parse(d);
            console.log(res);
            newTemplate('templates/selecionar-escola.html', {estados: res}, container);
        }
    });

}

function renderEditarNomeFoto() {
    newTemplate('templates/confirmar-nome-foto.html', null, container);
}

function renderInicio() {
    if (!validateUser())
        window.location.hash = '';
    $.ajax({
        url: host + 'app/get-posts-inicio',
        type: 'GET',
        data: {id: getUserData().id},
    })
        .done(function (d) {
            var data = convertToJSON(d);
            console.log('data inicio:', data);
            newTemplate('templates/inicio.html', data, container);
            console.log("success");
        })
        .fail(function (d) {
            console.log(d);
            console.log("error");
        });
}

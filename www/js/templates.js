var loadingTemplate = $.templates('' +
    '<div id="div-loading" class="center centralizar-elemento-na-tela">\n' +
    '    <div class="preloader-wrapper small active">\n' +
    '        <div class="spinner-layer spinner-blue-only">\n' +
    '            <div class="circle-clipper left">\n' +
    '                <div class="circle"></div>\n' +
    '            </div>\n' +
    '            <div class="gap-patch">\n' +
    '                <div class="circle"></div>\n' +
    '            </div>\n' +
    '            <div class="circle-clipper right">\n' +
    '                <div class="circle"></div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');

var loadingTemplateInsideElement = $.templates('' +
    '<div id="div-loading" class="center">\n' +
    '    <div class="preloader-wrapper small active">\n' +
    '        <div class="spinner-layer spinner-blue-only">\n' +
    '            <div class="circle-clipper left">\n' +
    '                <div class="circle"></div>\n' +
    '            </div>\n' +
    '            <div class="gap-patch">\n' +
    '                <div class="circle"></div>\n' +
    '            </div>\n' +
    '            <div class="circle-clipper right">\n' +
    '                <div class="circle"></div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
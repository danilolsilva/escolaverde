$.views.converters("tostr", function(val) {
	return JSON.stringify(val);
});

$.views.converters("formataData", function(val) {
	moment.locale('pt-br');
	return moment(val).format('DD/MM/YYYY HH:mm');
});

$.views.converters("getData", function(val) {
	moment.locale('pt-br');
	return moment(val).format('DD/MM/YYYY');
});

$.views.converters("getDataAgenda", function(val) {
	moment.locale('pt-br');
	return moment(val).format('DD [de] MMMM');
});

$.views.converters("getHora", function(val) {
	moment.locale('pt-br');
	return moment(val).format('HH:mm');
});

$.views.converters("position", function(val) {
	return parseInt(val) + 1;
});

$.views.converters("firstName", function(val) {
	return val.split(' ')[0];
});

$.views.converters("fotoPerfil", function(val) {
	if (val != null)
		return "style=\"background-image: url('" + host + "app/showImage?nome=" + val + "')\"";
	else
		return "";
});

$.views.converters("pontos", function(val) {
	if (val == null)
		return 0;
	else
		return val;
});

$.views.converters('getUrlFotoPerfil', function (fotoPerfil) {
	console.log('olha a foto ai ', fotoPerfil);
	return host + 'file/profile-picture?id=' + fotoPerfil.id + '&nome=' + fotoPerfil.nome;
});

$.views.converters('getUrlFotoPerfilByFotoPerfil', function (fotoPerfil) {
	if (fotoPerfil !== null && fotoPerfil != undefined)
		return host + 'file/profile-picture?id=' + fotoPerfil.id + '&nome=' + fotoPerfil.nome;
	return "";
});

